package com.podval;

public class Gauss extends SolvingMethod {


    public Gauss(double[][] matrix, double[] vector){

        super(matrix, vector);

    }


    public double[] solve(){


        //forward
        for (int k = 0; k < matrix.length; k++){

            for(int i = k + 1; i < matrix.length; i++){

                double m = matrix[i][k] / matrix[k][k];

                for(int j = k; j < matrix.length; j++){

                    matrix[i][j] -= m * matrix[k][j];

                }

                vector[i] -= m * vector[k];

            }

        }

        //backward

        result[matrix.length - 1] = vector[matrix.length - 1] / matrix[matrix.length - 1][ matrix.length - 1];

        for(int i = matrix.length - 2; i >= 0; i--){

            result[i] = vector[i];

            for( int j = i + 1; j < matrix.length; j++ ){

                result[i] -= matrix[i][j] * result[j];

            }

            result[i] /= matrix[i][i];

        }

        return result.clone();

    }
}
