package com.podval;

public class Cramer extends SolvingMethod {

    public Cramer(double[][] matrix, double[] vector){

        super(matrix, vector);

    }

    @Override
    public double[] solve() {


        double[] det_i = new double[result.length];
        double det = determinant(matrix);

        //debug
        System.out.println("matrix: ");
        for(double[] row : matrix){

            for(double elm : row) {
                System.out.print(elm + " ");
            }

            System.out.println();

        }

        double[] tmp = new double[result.length];

        for(int i = 0; i < result.length; i++){

            for(int j = 0; j < result.length; j++){

                if(i != 0){

                    matrix[j][i - 1] = tmp[j];

                }

                tmp[j] = matrix[j][i];

                matrix[j][i] = vector[j];

            }

            //debug
            System.out.println("matrix: ");
            for(double[] row : matrix){

                for(double elm : row) {
                    System.out.print(elm + " ");
                }

                System.out.println();

            }

            det_i[i] = determinant(matrix);

        }

        System.out.println();
        for(int i = 0; i < result.length; i++){

            System.out.println(" det[ " + i + "] = " + det_i[i]);
            System.out.println(" det = " + det);

            result[i] = det_i[i] / det;

        }

        return result.clone();

    }

    private double determinant(double[][] matrix){

        double[][] m = matrix.clone();

        for(int i = 0; i < matrix.length; i++){

            m[i] = matrix[i].clone();

        }


        for (int k = 0; k < m.length; k++){

            for(int i = k + 1; i < m.length; i++){

                double coeff = m[i][k] / m[k][k];

                for(int j = k; j < m.length; j++){

                    m[i][j] -= coeff * m[k][j];

                }

            }

        }

        double det = m[0][0];

        for(int i = 1; i < m.length; i++){

            det *= m[i][i];

        }

        return det;

    }
}
