package com.podval;

public class SimpleIterations extends IterationMethod {

    protected double tau = 0.5;


    SimpleIterations(double[][] matrix, double[] vector, double epsilon, double tau){

        super(matrix, vector, epsilon);

        this.tau = tau;

    }


    @Override
    protected void nextIter() {

        for(int i = 0; i < matrix.length; i++){

            double s = 0;

            for(int j = 0; j < matrix.length; j++){

                s += matrix[i][j] * xPrev[j];

            }

            s -= vector[i];

            xNext[i] = xPrev[i] - tau * s;

        }

    }
}
