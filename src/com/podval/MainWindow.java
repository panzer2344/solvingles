package com.podval;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {

    private JPanel footerPanel;
    private JPanel headerPanel;
    private JPanel lefterPanel;
    private JPanel righterPanel;
    private JPanel mainContentPanel;


    private JPanel lesInputPanel;

    private JPanel matInputPanel;
    private JPanel vecInputPanel;
    private JPanel precInputPanel;

    private JPanel resultPanel;


    private JScrollPane matScrollPanel;
    private JScrollPane vecScrollPanel;

    private JScrollPane resGaussScrollPanel;
    private JScrollPane resCramerScrollPanel;
    private JScrollPane resRelaxationScrollPanel;
    private JScrollPane resSimpleIterationScrollPanel;
    private JScrollPane resZeidelScrollPanel;


    private JPanel logPanel;
    private JPanel buttonsPanel;

    private JButton solveBtn;

    private JTextArea matTxtArea;
    private JTextArea vecTxtArea;


    private JTextArea resGaussTxtArea;
    private JTextArea resCramerTxtArea;
    private JTextArea resRelaxationTxtArea;
    private JTextArea resSimpleIterationTxtArea;
    private JTextArea resZeidelTxtArea;

    private JTextArea logTxtArea;

    private JTextField epsilonTextField;
    private JTextField tauTextField;
    private JTextField omegaTextField;

    private JLabel ALabel;
    private JLabel bLabel;

    private JLabel xGaussLabel;
    private JLabel xCramerLabel;
    private JLabel xRelaxationLabel;
    private JLabel xSimpleIterationLabel;
    private JLabel xZeidelLabel;

    private JLabel epsilonLabel;
    private JLabel tauLabel;
    private JLabel omegaLabel;

    private JLabel logLabel;


    private void initElements() {

        createPanels();

        createContent();

        initOuterLayout();

        initMainContentPanel();

        initRighterPanel();

        initLesInputPanel();

        initButtonsPanel();

        initLogPanel();

    }

    private void createPanels() {

        footerPanel = new JPanel();
        headerPanel = new JPanel();
        lefterPanel = new JPanel();
        righterPanel = new JPanel();
        mainContentPanel = new JPanel();

        lesInputPanel = new JPanel();
        matInputPanel = new JPanel();
        vecInputPanel = new JPanel();
        precInputPanel = new JPanel();

        resultPanel = new JPanel();

        logPanel = new JPanel();
        buttonsPanel = new JPanel();

    }

    private void createContent() {

        createButtons();

        createTxtAreas();

        createLabels();

        createScrollPanels();

    }

    private void createButtons() {
        solveBtn = new JButton("solve");
    }

    private void createTxtAreas() {
        matTxtArea = new JTextArea(10, 15);
        vecTxtArea = new JTextArea(10, 8);

        resGaussTxtArea = new JTextArea(10, 8);
        resCramerTxtArea = new JTextArea(10, 8);
        resRelaxationTxtArea = new JTextArea(10, 8);
        resSimpleIterationTxtArea = new JTextArea(10, 8);
        resZeidelTxtArea = new JTextArea(10, 8);

        logTxtArea = new JTextArea(10, 40);

        epsilonTextField = new JTextField();
        tauTextField = new JTextField();
        omegaTextField = new JTextField();

    }

    private void createLabels() {

        ALabel = new JLabel("A = ");
        bLabel = new JLabel("b = ");

        xGaussLabel = new JLabel("x(G) = ");
        xCramerLabel = new JLabel("x(C) = ");
        xRelaxationLabel = new JLabel("x(R) = ");
        xSimpleIterationLabel = new JLabel("x(SI) = ");
        xZeidelLabel = new JLabel("x(Z) = ");

        logLabel = new JLabel("Log: ");

        epsilonLabel = new JLabel("eps = ");
        tauLabel = new JLabel("tau = ");
        omegaLabel = new JLabel("omega = ");

    }

    private void createScrollPanels() {

        matScrollPanel = new JScrollPane(matTxtArea);
        vecScrollPanel = new JScrollPane(vecTxtArea);

        resGaussScrollPanel = new JScrollPane(resGaussTxtArea);
        resCramerScrollPanel = new JScrollPane(resCramerTxtArea);
        resRelaxationScrollPanel = new JScrollPane(resRelaxationTxtArea);
        resSimpleIterationScrollPanel = new JScrollPane(resSimpleIterationTxtArea);
        resZeidelScrollPanel = new JScrollPane(resZeidelTxtArea);

    }


    private void initOuterLayout() {

        getContentPane().add(footerPanel, BorderLayout.SOUTH);
        getContentPane().add(headerPanel, BorderLayout.NORTH);
        getContentPane().add(lefterPanel, BorderLayout.WEST);
        getContentPane().add(righterPanel, BorderLayout.EAST);
        getContentPane().add(mainContentPanel, BorderLayout.CENTER);

    }

    private void initMainContentPanel() {

        mainContentPanel.setLayout(new GridLayout(2, 1));

        mainContentPanel.add(lesInputPanel);
        mainContentPanel.add(logPanel);

    }

    private void initRighterPanel() {

        righterPanel.add(buttonsPanel);

    }

    private void initLesInputPanel() {

        lesInputPanel.setLayout(new FlowLayout());

        lesInputPanel.add(matInputPanel);
        lesInputPanel.add(vecInputPanel);

        lesInputPanel.add(precInputPanel);

        lesInputPanel.add(resultPanel);

        matInputPanel.add(ALabel);
        matInputPanel.add(matScrollPanel);

        vecInputPanel.add(bLabel);
        vecInputPanel.add(vecScrollPanel);

        resultPanel.add(xGaussLabel);
        resultPanel.add(resGaussScrollPanel);

        resultPanel.add(xCramerLabel);
        resultPanel.add(resCramerScrollPanel);

        resultPanel.add(xRelaxationLabel);
        resultPanel.add(resRelaxationScrollPanel);

        resultPanel.add(xSimpleIterationLabel);
        resultPanel.add(resSimpleIterationScrollPanel);

        resultPanel.add(xZeidelLabel);
        resultPanel.add(resZeidelScrollPanel);

    }

    private void initButtonsPanel() {

        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
        buttonsPanel.add(solveBtn);

    }

    private void initLogPanel() {

        logPanel.setLayout(new BoxLayout(logPanel, BoxLayout.Y_AXIS));

        logPanel.add(logLabel);
        logPanel.add(logTxtArea);

    }

    private void createListeners() {

        solveBtn.addActionListener((e) -> {

            double[][] matrix = null;
            double[] b = null;

            try {

                matrix = readMatrixFromTA();

                b = readVector(vecTxtArea);

                System.out.println("matrix: ");
                for (double[] row : matrix) {

                    for (double elm : row) {

                        System.out.print(elm + " ");

                    }

                    System.out.println();

                }


                System.out.println("vector: ");
                for (double elm : b) {

                    System.out.print(elm + " ");

                }
                System.out.println();

                SolvingMethod gauss = new Gauss(matrix, b);
                SolvingMethod cramer = new Cramer(matrix, b);

                SolvingMethod relax = new RelaxationMethod(matrix, b, 0.5, 0.0001);
                SolvingMethod zeidel = new ZeidelMethod(matrix, b, 0.0001);
                SolvingMethod simpleIter = new SimpleIterations(matrix, b, 0.0001, 0.0025);

                writeToVecTxtArea(gauss, resGaussTxtArea);
                writeToVecTxtArea(cramer, resCramerTxtArea);
                writeToVecTxtArea(relax, resRelaxationTxtArea);
                writeToVecTxtArea(zeidel, resZeidelTxtArea);
                writeToVecTxtArea(simpleIter, resSimpleIterationTxtArea);

                logTxtArea.append(" zeidel method finished on " + ((ZeidelMethod) zeidel).getOperCnt() + " operations\n");
                logTxtArea.append(" relaxation method finished on " + ((RelaxationMethod) relax).getOperCnt() + " operations\n");
                logTxtArea.append(" simple iterations method finished on " + ((SimpleIterations) simpleIter).getOperCnt() + " operations\n");


            } catch (NumberFormatException nfe) {

                System.out.println(nfe.getMessage());
                //ex.printStackTrace();

                logTxtArea.append(nfe.getMessage() + "\n");

            } catch (Exception ex) {

                System.out.println(ex.getMessage());
                //ex.printStackTrace();

                logTxtArea.append(ex.getMessage() + "\n");

            }

        });

    }

    private void writeToVecTxtArea(SolvingMethod method, JTextArea txtArea){

        txtArea.setText("");

        for (double elm : method.solve()) {

            txtArea.append("" + elm + "\n");

            if(elm == Double.POSITIVE_INFINITY || elm == Double.NEGATIVE_INFINITY){

                logTxtArea.append( method.getClass().getName() + " does not coverage \n " );

                break;
            }

        }

    }

        private double[][] readMatrixFromTA () throws Exception {

            String areaContent = matTxtArea.getText();

            if (areaContent.equals("")) {

                throw new Exception(" Error: Input matrix to MatrixTextArea ");

            }


            String[] rows = areaContent.split("\n");

            double[][] matrix = new double[rows.length][rows.length];

            for (int i = 0; i < rows.length; i++) {

                String[] strElms = rows[i].split(" ");

                if (strElms.length != rows.length) {

                    throw new Exception("Error: matrix isn't square");

                }

                for (int j = 0; j < strElms.length; j++) {

                    try {

                        matrix[i][j] = Double.parseDouble(strElms[j]);

                    } catch (NumberFormatException nfe) {

                        throw new NumberFormatException(" Input valid values in MatrixTextArea \n ");

                    }

                }

            }

            return matrix;

        }

        private double[] readVector (JTextArea txtArea) throws Exception {

            String areaCnt = txtArea.getText();

            if (areaCnt.equals("")) {

                throw new Exception(" Error: Input matrix to MatrixTextArea ");

            }

            String[] strElms = areaCnt.split("\n");

            double[] vector = new double[strElms.length];

            for (int i = 0; i < strElms.length; i++) {

                try {

                    vector[i] = Double.parseDouble(strElms[i]);

                } catch (NumberFormatException nfe) {

                    throw new NumberFormatException(" Input valid values in VectorTextArea");

                }

            }

            return vector;

        }


        MainWindow() {

            super("Solving LES");

            setLayout(new BorderLayout());
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setPreferredSize(new Dimension(800, 800));

            initElements();

            createListeners();

            pack();
            setVisible(true);

        }

    }
