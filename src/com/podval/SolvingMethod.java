package com.podval;

public abstract class SolvingMethod {

    protected double[][] matrix;

    protected double[] vector;

    protected double[] result;

    public SolvingMethod(double[][] matrix, double[] vector){

        this.matrix = matrix.clone();

        for(int i = 0; i < matrix.length ; i++){

            this.matrix[i] = matrix[i].clone();

        }

        this.vector = vector.clone();

        result = new double[vector.length];

    }

    public abstract double[] solve();

    public double[] getResult(){

        return result.clone();

    }

}
