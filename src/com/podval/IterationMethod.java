package com.podval;

public abstract class IterationMethod extends SolvingMethod {

    protected double epsilon = 0.5;

    protected double[] xPrev = null;
    protected double[] xNext = null;

    protected double[][] B = null;
    protected double[] C = null;

    private int operCnt = 0;

    IterationMethod(double[][] matrix, double[] vector, double epsilon){

        super(matrix, vector);

        this.epsilon = epsilon;

        this.xPrev = new double[vector.length];
        this.xNext = new double[vector.length];

        this.B = new double[vector.length][vector.length];
        this.C = new double[vector.length];

    }


    @Override
    public double[] solve() {

        int n = matrix.length;
        double diff = 0;


        for(int i = 0; i < n; i++){

            for(int j = 0; j < n; j++){

                if( i == j ) {
                    B[i][j] = 0;
                }

                B[i][j] = - matrix[i][j] / matrix[i][i];
            }

            C[i] = vector[i] / matrix[i][i];

        }

        do{

            nextIter();

            diff = 0;
            for (int i = 0; i < n; i++) {

                double this_diff = Math.abs(xNext[i] - xPrev[i]);

                if(this_diff > diff){

                    diff = this_diff;

                }

            }

            xPrev = xNext.clone();

            operCnt++;

            System.out.println("on operation #" + operCnt);
            for(int i = 0; i < n; i++){
                System.out.print("res[" + i + "] = " + xNext[i] + " ");
            }
            System.out.println();
            System.out.println( "diff = " + diff + "eps = " + epsilon);


        }while(diff > epsilon && operCnt < 10000);

        result = xNext.clone();

        return result.clone();

    }


    protected abstract void nextIter();

    public int getOperCnt() {
        return operCnt;
    }
}
