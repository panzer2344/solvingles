package com.podval;

public class RelaxationMethod extends IterationMethod {

    protected double omega = 1;

    RelaxationMethod(double[][] matrix, double[] vector, double omega, double epsilon){

        super(matrix, vector, epsilon);

        this.omega = omega;

    }

    @Override
    protected void nextIter() {

        int n = matrix.length;

        for(int i = 0; i < n; i++){

            double s1 = 0, s2 = 0;

            for(int j = 0; j < i; j++){

                s1 += B[i][j] * xNext[j];

            }

            for(int j = i + 1; j < n; j++){

                s2 += B[i][j] * xPrev[j];

            }

            xNext[i] = (1 - omega) * xPrev[i] + omega * (C[i] + s1 + s2);

        }

    }
}
